﻿using System;
using System.Web.Http;
using CurrencyConverter.Common.Models;
using CurrencyConverter.Server.BusinessLogic.Interfaces;

namespace CurrencyConverter.Server.Services.Controllers
{
    public class ExchangeRatesController : ApiController
    {
        private IExchangeRatesCacher ExchangeRatesCacher { get; }
        private ICurrencyConverter Converter { get; }

        public ExchangeRatesController(IExchangeRatesCacher exchangeRatesCacher, ICurrencyConverter converter)
        {
            ExchangeRatesCacher = exchangeRatesCacher;
            Converter = converter;
        }

        public CommonExchangeRate Get(DateTime date, string @base)
        {
            if (date == null)
                throw new NotImplementedException();

            if (string.IsNullOrEmpty(@base))
                throw new NotImplementedException();

            var rates = ExchangeRatesCacher.LoadExchangeData(date);

            if (!@base.Equals(rates.Base))
                rates = Converter
                    .ConvertRatesToBase(rates, @base);

            return rates.MapToCommon();
        }
    }
}