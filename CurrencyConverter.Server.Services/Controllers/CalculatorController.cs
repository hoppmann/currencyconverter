﻿using System;
using System.Web.Http;
using CurrencyConverter.Server.BusinessLogic.Interfaces;

namespace CurrencyConverter.Server.Services.Controllers
{
    public class CalculatorController : ApiController
    {
        private ICurrencyConverter CurrencyConverter { get; }

        public CalculatorController(ICurrencyConverter currencyConverter)
        {
            CurrencyConverter = currencyConverter;
        }

        public decimal Get(string from, string to, decimal value)
        {
            if (string.IsNullOrEmpty(from) | string.IsNullOrEmpty(to))
                throw new NotImplementedException();

            return CurrencyConverter.ConvertCurrencyValue(value, from, to);
        }
    }
}