﻿using System.Collections.Generic;
using System.Linq;
using CurrencyConverter.Common.Models;
using CurrencyConverter.Server.BusinessLogic.Models;

namespace CurrencyConverter.Server.Services
{
    public static class ServiceModelMapper
    {
        public static CommonExchangeRate MapToCommon(this ExchangeRatesBusinessModel entitie)
        {
            return new CommonExchangeRate()
            {
                Base = entitie.Base,
                Date = entitie.Date,
                Rates = entitie.Rates.MapToCommon().ToList()
            };
        }

        public static ExchangeRatesBusinessModel MapToBusiness(this CommonExchangeRate entitie)
        {
            return new ExchangeRatesBusinessModel
            {
                Base = entitie.Base,
                Date = entitie.Date,
                Rates = entitie.Rates.MapToBusiness().ToList()
            };
        }
        private static IEnumerable<CommonCurrencyExchange> MapToCommon(this IEnumerable<CurrencyRateBusinessModel> entities)
        {
            return entities.Select(entity => new CommonCurrencyExchange
            {
                CurrencyName = entity.CurrencyName,
                Rate = entity.Rate
            });
        }

        private static IEnumerable<CurrencyRateBusinessModel> MapToBusiness(
            this IEnumerable<CommonCurrencyExchange> entities)
        {
            return entities.Select(entity => new CurrencyRateBusinessModel{CurrencyName = entity.CurrencyName, Rate = entity.Rate});
        }
    }
}