﻿using System.Web.Http;

namespace CurrencyConverter.Server.Services
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var container = new IocContainer();
            container.Initialze();
        }
    }
}
