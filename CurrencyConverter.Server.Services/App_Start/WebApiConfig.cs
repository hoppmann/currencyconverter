﻿using System.Web.Http;

namespace CurrencyConverter.Server.Services
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{option}",
                defaults: new { option = RouteParameter.Optional }
            );
        }
    }
}
