﻿using System;
using System.Reflection;
using Autofac;
using System.Web.Http;
using Autofac.Integration.WebApi;
using CurrencyConverter.Server.BusinessLogic;

namespace CurrencyConverter.Server.Services
{
    public class IocContainer
    {
        public void Initialze()
        {
            Configure();

            var config = GlobalConfiguration.Configuration;

            config.DependencyResolver = new AutofacWebApiDependencyResolver(Container);

        }

        private IContainer Container { get; set; }

        protected void Configure()
        {
            var builder = new ContainerBuilder();
        
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            BusinesLogicContainer.RegisterTypes(builder);


            Container = builder.Build();
        }

        protected object GetInstance(Type serviceType, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                if (Container.IsRegistered(serviceType))
                    return Container.Resolve(serviceType);
            }
            else
            {
                if (Container.IsRegisteredWithName(key, serviceType))
                    return Container.ResolveNamed(key, serviceType);
            }
            throw new Exception($"Could not locate any instances of contract {key ?? serviceType.Name}.");
        }
    }
}