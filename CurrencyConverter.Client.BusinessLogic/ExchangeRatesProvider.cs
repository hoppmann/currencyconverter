﻿using System.IO;
using CurrencyConverter.Client.BusinessLogic.Interfaces;
using CurrencyConverter.Client.BusinessLogic.Models;

namespace CurrencyConverter.Client.BusinessLogic
{
    public class ExchangeRatesProvider : IExchangeRatesProvider
    {
        public ExchangeRatesProvider(
            IOnlineCurrencyExchangeDataProvider onlineCurrencyExchangeDataProvider)
        {
            OnlineCurrencyExchangeDataProvider = onlineCurrencyExchangeDataProvider;
        }

        private IOnlineCurrencyExchangeDataProvider OnlineCurrencyExchangeDataProvider { get; }


        public bool HasData()
        {
            var onlineDataAvailable = OnlineCurrencyExchangeDataProvider.OnlineDataAvailable();

            return onlineDataAvailable;
        }

        public ExchangeRatesBusinessModel LoadExchangeData()
        {
            if (!HasData())
                throw new InvalidDataException(
                    "No Data available, please make sure to enable internet connection, or switch onlineservice if possible");

            return  OnlineCurrencyExchangeDataProvider.LoadExchangeData();
            
        }
    }
}