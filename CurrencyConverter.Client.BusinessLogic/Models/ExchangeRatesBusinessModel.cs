﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Client.BusinessLogic.Models
{
    public class ExchangeRatesBusinessModel
    {
        public string Base { get; set; }
        public DateTime Date { get; set; }
        public IList<CurrencyExchangeBusinessModel> Rates { get; set; }
    }
}