﻿namespace CurrencyConverter.Client.BusinessLogic.Models
{
    public class CurrencyExchangeBusinessModel
    {
        public string CurrencyName { get; set; }
        public decimal Rate { get; set; }
    }
}