﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Client.BusinessLogic.Models
{
    public class FixerJsonLayout
    {
        public string Base { get; set; }
        public DateTime Date { get; set; }
        public IDictionary<string, decimal> Rates { get; set; }
    }
}