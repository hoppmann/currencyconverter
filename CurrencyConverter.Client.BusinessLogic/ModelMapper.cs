﻿using System.Collections.Generic;
using System.Linq;
using CurrencyConverter.Client.BusinessLogic.Models;
using CurrencyConverter.Client.DataAcces.Models;

namespace CurrencyConverter.Client.BusinessLogic
{
    public static class ModelMapper
    {
        public static ExchangeRatesBusinessModel Map(this ExchangeRatesEntitie entitie)
        {
            return new ExchangeRatesBusinessModel
            {
                Base = entitie.Base,
                Date = entitie.Date,
                Rates = Map((IEnumerable<CurrencyExchangeEntitie>) entitie.Rates).ToList()
            };
        }

        public static ExchangeRatesEntitie Map(this ExchangeRatesBusinessModel entitie)
        {
            return new ExchangeRatesEntitie
            {
                Base = entitie.Base,
                Date = entitie.Date,
                Rates = entitie.Rates.Map().ToList()
            };
        }
        private static IEnumerable<CurrencyExchangeEntitie> Map(this IEnumerable<CurrencyExchangeBusinessModel> entities)
        {
            return entities.Select(entity => new CurrencyExchangeEntitie
            {
                CurrencyName = entity.CurrencyName,
                Rate = entity.Rate
            });
        }

        private static IEnumerable<CurrencyExchangeBusinessModel> Map(
            this IEnumerable<CurrencyExchangeEntitie> entities)
        {
            return entities.Select(entity => new CurrencyExchangeBusinessModel{CurrencyName = entity.CurrencyName, Rate = entity.Rate});
        }
    }
}