﻿using System.Collections.Generic;

namespace CurrencyConverter.Client.BusinessLogic.Interfaces
{
    public interface ICurrencyDataProvider
    {
        IList<string> LoadCurrencyData();
    }
}
