﻿using System;

namespace CurrencyConverter.Client.BusinessLogic.Interfaces
{
    public interface IOnlineCurrencyExchangeDataProvider : ICurrencyDataProvider, IExchangeDataProvider
    {
        bool OnlineDataAvailable();

        bool NewerDataAvailable(DateTime currentDateTime);
    }
}
