﻿namespace CurrencyConverter.Client.BusinessLogic.Interfaces
{
    public interface ICurrencyConverter
    {
        decimal ConvertCurrencyValue(decimal sourceCurrencyValue, string source, string target);
    }
}
