﻿using CurrencyConverter.Client.BusinessLogic.Models;

namespace CurrencyConverter.Client.BusinessLogic.Interfaces
{
    public interface IExchangeDataProvider
    {
        ExchangeRatesBusinessModel LoadExchangeData();
    }
}