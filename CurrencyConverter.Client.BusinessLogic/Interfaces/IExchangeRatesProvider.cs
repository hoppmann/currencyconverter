﻿namespace CurrencyConverter.Client.BusinessLogic.Interfaces
{
    public interface IExchangeRatesProvider : IExchangeDataProvider
    {
        bool HasData();
    }
}