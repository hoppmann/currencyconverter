﻿using System;
using System.Collections.Generic;
using System.Linq;
using CurrencyConverter.Client.BusinessLogic.Interfaces;
using CurrencyConverter.Client.BusinessLogic.Models;
using CurrencyConverter.Client.DataAcces.Interfaces;

namespace CurrencyConverter.Client.BusinessLogic
{
    public class CurrencyConverterIo : IOnlineCurrencyExchangeDataProvider
    {
        public IApiAcces ApiAcces { get; }

        public CurrencyConverterIo(IApiAcces apiAcces)
        {
            ApiAcces = apiAcces;
        }
        public IList<string> LoadCurrencyData()
        {
            return ApiAcces.GetExchangeRates().Rates.Select(x => x.CurrencyName).ToList();
        }

        public ExchangeRatesBusinessModel LoadExchangeData()
        {
            return ApiAcces.GetExchangeRates().Map();
        }

        public bool OnlineDataAvailable()
        {
            return ApiAcces.DataAvailable();
        }

        public bool NewerDataAvailable(DateTime currentDateTime)
        {
            return ApiAcces.NewerDataAvailable(currentDateTime);
        }
    }
}