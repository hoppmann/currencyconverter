﻿using Autofac;
using CurrencyConverter.Client.BusinessLogic.Interfaces;
using CurrencyConverter.Client.DataAcces;

namespace CurrencyConverter.Client.BusinessLogic
{
    public static class BusinesLogicContainer
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            ////builder.RegisterType<CurrencyConversionCalculator>().As<ICurrencyConversionCalculator>();
            builder.RegisterType<CurrencyValueConverter>().As<ICurrencyConverter>();
            builder.RegisterType<CurrencyConverterIo>().As<IOnlineCurrencyExchangeDataProvider>();
            builder.RegisterType<ExchangeRatesProvider>().As<IExchangeRatesProvider>();

            DataAccesContainer.RegisterTypes(builder);
        }
    }
}