﻿using CurrencyConverter.Client.BusinessLogic.Interfaces;
using CurrencyConverter.Client.DataAcces.Interfaces;

namespace CurrencyConverter.Client.BusinessLogic
{
    public class CurrencyValueConverter : ICurrencyConverter
    {
        private IApiAcces ApiAcces { get; }

        public CurrencyValueConverter(IApiAcces apiAcces)
        {
            ApiAcces = apiAcces;
        }

        public decimal ConvertCurrencyValue(decimal sourceCurrencyValue, string source, string target)
        {
            return ApiAcces.GetExchangeResult(sourceCurrencyValue, source, target);
        }
    }
}