﻿using Autofac;
using CurrencyConverter.Server.BusinessLogic.Interfaces;
using CurrencyConverter.Server.DataAcces;

namespace CurrencyConverter.Server.BusinessLogic
{
    public static class BusinesLogicContainer
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<CurrencyConversionCalculator>().As<ICurrencyConversionCalculator>();
            builder.RegisterType<CurrencyConverter>().As<ICurrencyConverter>();
            builder.RegisterType<ExchangeRatesCacher>().As<IExchangeRatesCacher>();
            builder.RegisterType<OfflineCurrencyExchangeDataProvider>().As<IOfflineCurrencyExchangeDataProvider>();
            builder.RegisterType<FixerIo>().As<IOnlineCurrencyExchangeDataProvider>();

            DataAccesContainer.RegisterTypes(builder);
        }
    }
}