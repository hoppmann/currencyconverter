﻿using System.Collections.Generic;
using System.Linq;
using CurrencyConverter.Server.BusinessLogic.Models;
using CurrencyConverter.Server.DataAcces.Models;

namespace CurrencyConverter.Server.BusinessLogic
{
    public static class BusinessModelMapper
    {
        public static ExchangeRatesBusinessModel MapToBusiness(this ExchangeRatesEntity entity)
        {
            return new ExchangeRatesBusinessModel
            {
                Base = entity.Base,
                Date = entity.Date,
                Rates = entity.Rates.MapToBusiness().ToList()
            };
        }

        public static ExchangeRatesEntity MapToEntity(this ExchangeRatesBusinessModel entitie)
        {
            return new ExchangeRatesEntity
            {
                Base = entitie.Base,
                Date = entitie.Date,
                Rates = entitie.Rates.MapToEntity().ToList()
            };
        }
        private static IEnumerable<CurrencyExchangeEntity> MapToEntity(this IEnumerable<CurrencyRateBusinessModel> entities)
        {
            return entities.Select(entity => new CurrencyExchangeEntity
            {
                CurrencyName = entity.CurrencyName,
                Rate = entity.Rate
            });
        }

        private static IEnumerable<CurrencyRateBusinessModel> MapToBusiness(
            this IEnumerable<CurrencyExchangeEntity> entities)
        {
            return entities.Select(entity => new CurrencyRateBusinessModel{CurrencyName = entity.CurrencyName, Rate = entity.Rate});
        }
    }
}