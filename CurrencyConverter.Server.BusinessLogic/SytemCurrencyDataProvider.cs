﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CurrencyConverter.Server.BusinessLogic.Interfaces;

namespace CurrencyConverter.Server.BusinessLogic
{
    public class SytemCurrencyDataProvider : ICurrencyDataProvider
    {
        public IList<string> LoadCurrencyData()
        {
            var currencyNameInfos = LoadCurrencyNameInfos();
            var isoCurrencySymbolList = currencyNameInfos.Select(x => x.Value).ToList();

            return isoCurrencySymbolList;
        }

        private Dictionary<string, string> LoadCurrencyNameInfos()
        {
            var currencyNameInfos = new Dictionary<string, string>();
            foreach (var info in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                var regionInfo = new RegionInfo(info.LCID);
                if (!currencyNameInfos.ContainsKey(regionInfo.ISOCurrencySymbol))
                    currencyNameInfos.Add(regionInfo.ISOCurrencySymbol, regionInfo.CurrencyEnglishName);
            }

            return currencyNameInfos;
        }
    }
}