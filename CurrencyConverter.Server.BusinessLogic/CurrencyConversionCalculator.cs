﻿using CurrencyConverter.Server.BusinessLogic.Interfaces;

namespace CurrencyConverter.Server.BusinessLogic
{
    public class CurrencyConversionCalculator : ICurrencyConversionCalculator
    {
        public decimal ExchangeRate(decimal ratioSource, decimal ratioTarget)
        {
            return ratioSource / ratioTarget;
        }

        public decimal ValueToRatio(decimal valueSource, decimal ratioTarget)
        {
            return valueSource / ratioTarget;
        }
    }
}