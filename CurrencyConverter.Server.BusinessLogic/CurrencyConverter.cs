﻿using System;
using System.Collections.Generic;
using System.Linq;
using CurrencyConverter.Server.BusinessLogic.Exceptions;
using CurrencyConverter.Server.BusinessLogic.Interfaces;
using CurrencyConverter.Server.BusinessLogic.Models;

namespace CurrencyConverter.Server.BusinessLogic
{
    public class CurrencyConverter : ICurrencyConverter
    {
        private IExchangeRatesCacher ExchangeRatesCacher { get; }
        private ICurrencyConversionCalculator CurrencyConversionCalculator { get; }


        public CurrencyConverter(ICurrencyConversionCalculator currencyConversionCalculator,
            IExchangeRatesCacher exchangeRatesCacher)
        {
            CurrencyConversionCalculator = currencyConversionCalculator;
            ExchangeRatesCacher = exchangeRatesCacher;
        }

        public decimal ConvertCurrencyValue(decimal sourceCurrencyValue, string sourceCurrency, string targetCurrency)
        {
            IList<string> givenNames = new List<string> {sourceCurrency, targetCurrency};
            var rates = ExchangeRatesCacher.LoadExchangeData(DateTime.Today).Rates
                .Where(x => givenNames.Contains(x.CurrencyName)).ToList();

            if (rates.Count < 2)
                throw new InvalidCurrencyException();

            var sourceRate = ExtractRateFromName(sourceCurrency, rates);
            var targetRate = ExtractRateFromName(targetCurrency, rates);

            var exchangeRateBetweenCurrencies = CurrencyConversionCalculator.ExchangeRate(sourceRate, targetRate);

            return CurrencyConversionCalculator.ValueToRatio(sourceCurrencyValue, exchangeRateBetweenCurrencies);
        }

        public ExchangeRatesBusinessModel ConvertRatesToBase(ExchangeRatesBusinessModel exchangeRates,
            string newBaseCurrency)
        {
            if (!exchangeRates.Rates.Any(x => x.CurrencyName.Equals(newBaseCurrency)))
                throw new InvalidCurrencyException();

            var newBase = exchangeRates.Rates.Single(x => x.CurrencyName.Equals(newBaseCurrency));
            exchangeRates.Base = newBase.CurrencyName;

            foreach (var rate in exchangeRates.Rates)
                rate.Rate = CurrencyConversionCalculator.ExchangeRate(rate.Rate, newBase.Rate);

            return exchangeRates;
        }

        private decimal ExtractRateFromName(string currencyName, IEnumerable<CurrencyRateBusinessModel> rates)
        {
            return rates.Where(x => x.CurrencyName.Equals(currencyName)).Select(x => x.Rate).Single();
        }
    }
}