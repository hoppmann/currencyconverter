﻿namespace CurrencyConverter.Server.BusinessLogic.Models
{
    public class CurrencyRateBusinessModel
    {
        public string CurrencyName { get; set; }
        public decimal Rate { get; set; }
    }
}