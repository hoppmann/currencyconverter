﻿namespace CurrencyConverter.Server.BusinessLogic.Models
{
    public class @string
    {
        public string CurrencyName { get; set; }
        public decimal Rate { get; set; }
    }
}