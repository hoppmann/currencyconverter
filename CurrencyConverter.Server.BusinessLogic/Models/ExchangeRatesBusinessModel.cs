﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Server.BusinessLogic.Models
{
    public class ExchangeRatesBusinessModel
    {
        public string Base { get; set; }
        public DateTime Date { get; set; }
        public IList<CurrencyRateBusinessModel> Rates { get; set; }
    }
}