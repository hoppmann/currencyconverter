﻿using System;
using System.IO;
using CurrencyConverter.Server.BusinessLogic.Interfaces;
using CurrencyConverter.Server.BusinessLogic.Models;

namespace CurrencyConverter.Server.BusinessLogic
{
    public class ExchangeRatesCacher : IExchangeRatesCacher
    {
        public ExchangeRatesCacher(
            IOnlineCurrencyExchangeDataProvider onlineCurrencyExchangeDataProvider,
            IOfflineCurrencyExchangeDataProvider offlineCurrencyExchangeDataProvider)
        {
            OnlineCurrencyExchangeDataProvider = onlineCurrencyExchangeDataProvider;
            OfflineCurrencyExchangeDataProvider = offlineCurrencyExchangeDataProvider;
        }

        private IOnlineCurrencyExchangeDataProvider OnlineCurrencyExchangeDataProvider { get; }

        private IOfflineCurrencyExchangeDataProvider OfflineCurrencyExchangeDataProvider { get; }

        public bool HasData()
        {
            var onlineDataAvailable = OnlineCurrencyExchangeDataProvider.OnlineDataAvailable();
            var offlineDataAvailable = OfflineCurrencyExchangeDataProvider.DataAvailable();

            return onlineDataAvailable || offlineDataAvailable;
        }

        public ExchangeRatesBusinessModel LoadExchangeData()
        {
            throw new NotImplementedException();
        }

        public ExchangeRatesBusinessModel LoadExchangeData(DateTime specificDateTime)
        {
            if (!HasData())
                throw new InvalidDataException(
                    "No Data available, please make sure to enable internet connection, or switch onlineservice if possible");

            var onlineData = OnlineCurrencyExchangeDataProvider.LoadExchangeData(specificDateTime);

            if (onlineData == null)
                return OfflineCurrencyExchangeDataProvider.LoadExchangeData(specificDateTime);

            OfflineCurrencyExchangeDataProvider.SaveData(onlineData);
            return onlineData;
        }
    }
}