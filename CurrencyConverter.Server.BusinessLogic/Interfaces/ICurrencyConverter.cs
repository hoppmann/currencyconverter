﻿using CurrencyConverter.Server.BusinessLogic.Models;

namespace CurrencyConverter.Server.BusinessLogic.Interfaces
{
    public interface ICurrencyConverter
    {
        decimal ConvertCurrencyValue(decimal sourceCurrencyValue, string sourceCurrency, string targetCurrency);
        ExchangeRatesBusinessModel ConvertRatesToBase(ExchangeRatesBusinessModel exchangeRates, string newBaseCurrency);
    }
}
