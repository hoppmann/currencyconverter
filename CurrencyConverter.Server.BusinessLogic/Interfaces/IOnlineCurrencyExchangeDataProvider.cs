﻿using System;

namespace CurrencyConverter.Server.BusinessLogic.Interfaces
{
    public interface IOnlineCurrencyExchangeDataProvider : ICurrencyDataProvider, IExchangeDataProvider
    {
        bool OnlineDataAvailable();

        bool NewerDataAvailable(DateTime currentDateTime);
    }
}
