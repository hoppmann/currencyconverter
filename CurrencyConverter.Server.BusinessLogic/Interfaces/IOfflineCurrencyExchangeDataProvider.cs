﻿using CurrencyConverter.Server.BusinessLogic.Models;

namespace CurrencyConverter.Server.BusinessLogic.Interfaces
{
    public interface IOfflineCurrencyExchangeDataProvider : IExchangeDataProvider
    {
        bool DataAvailable();

        bool SaveData(ExchangeRatesBusinessModel exchangeData);
    }
}