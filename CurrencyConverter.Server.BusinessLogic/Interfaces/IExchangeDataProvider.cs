﻿using System;
using CurrencyConverter.Server.BusinessLogic.Models;

namespace CurrencyConverter.Server.BusinessLogic.Interfaces
{
    public interface IExchangeDataProvider
    {
        ExchangeRatesBusinessModel LoadExchangeData(DateTime specificDateTime);
    }
}