﻿using System.Collections.Generic;

namespace CurrencyConverter.Server.BusinessLogic.Interfaces
{
    public interface ICurrencyDataProvider
    {
        IList<string> LoadCurrencyData();
    }
}
