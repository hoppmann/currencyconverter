﻿namespace CurrencyConverter.Server.BusinessLogic.Interfaces
{
    public interface IExchangeRatesCacher : IExchangeDataProvider
    {
        bool HasData();
    }
}