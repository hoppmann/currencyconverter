﻿namespace CurrencyConverter.Server.BusinessLogic.Interfaces
{
    public interface ICurrencyConversionCalculator
    {
        decimal ExchangeRate(decimal ratioSource, decimal ratioTarget);
        decimal ValueToRatio(decimal valueSource, decimal ratioTarget);
    }
}