﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using CurrencyConverter.Server.BusinessLogic.Interfaces;
using CurrencyConverter.Server.BusinessLogic.Models;
using Newtonsoft.Json;

namespace CurrencyConverter.Server.BusinessLogic
{
    public class FixerIo : IOnlineCurrencyExchangeDataProvider
    {
        private const string StandartCurrencyBase = "USD";
        private const string FixerUrlBase = "https://api.fixer.io/";

        private string FixerUrl { get; set; }

        private string ConfigFixerUrl(string baseCurrency, DateTime specificDateTime)
        {
            var date = specificDateTime.ToString("yyyy-MM-dd");

            FixerUrl = FixerUrlBase + $"{date}?base={baseCurrency.ToUpper()}";
            return FixerUrl;
        }
        public IList<string> LoadCurrencyData()
        {
            var data = GetDataFromWeb(DateTime.Today);

            var isoCurrencySymbolList = data.Rates.Select(x => x.Key).ToList();
            isoCurrencySymbolList.Add(data.Base);

            return isoCurrencySymbolList;
        }

        public bool OnlineDataAvailable()
        {
            return GetDataFromWeb(DateTime.Today) != null;
        }

        public bool NewerDataAvailable(DateTime currentDateTime)
        {
            return GetDataFromWeb(DateTime.Today).Date.Subtract(currentDateTime) > TimeSpan.Zero;
        }
        
        public ExchangeRatesBusinessModel LoadExchangeData(DateTime specificDateTime)
        {
            var data = GetDataFromWeb(specificDateTime);

            var exchangeValue = data.Rates;
            exchangeValue.Add(data.Base, decimal.One);

            return new ExchangeRatesBusinessModel
            {
                Base = data.Base,
                Date = data.Date,
                Rates = data.Rates.Select(x => new CurrencyRateBusinessModel() {CurrencyName = x.Key, Rate = x.Value})
                    .ToList()
            };
        }

        private FixerJsonLayout GetDataFromWeb(DateTime specificDateTime, string baseCurrency = StandartCurrencyBase)
        {
            using (WebClient client = new WebClient())
            {
                ConfigFixerUrl(baseCurrency, specificDateTime);
                
                var result = client.DownloadString(FixerUrl);

                var latestFixerCurrencyJsonData = JsonConvert.DeserializeObject<FixerJsonLayout>(result);

                return latestFixerCurrencyJsonData;
            }
        }
    }
}