using System;
using CurrencyConverter.Server.BusinessLogic.Interfaces;
using CurrencyConverter.Server.BusinessLogic.Models;
using CurrencyConverter.Server.DataAcces.Interfaces;

namespace CurrencyConverter.Server.BusinessLogic
{
    public class OfflineCurrencyExchangeDataProvider : IOfflineCurrencyExchangeDataProvider
    {
        public OfflineCurrencyExchangeDataProvider(IExchangeRatesRepository repository)
        {
            Repository = repository;
        }

        private IExchangeRatesRepository Repository { get; }

        public ExchangeRatesBusinessModel LoadExchangeData()
        {
            return LoadExchangeData(DateTime.Today);
        }

        public ExchangeRatesBusinessModel LoadExchangeData(DateTime specificDateTime)
        {
            return Repository.Load(specificDateTime).MapToBusiness();
        }

        public bool DataAvailable()
        {
            return Repository.DataAvailable();
        }

        public bool SaveData(ExchangeRatesBusinessModel exchangeData)
        {
            return Repository.Save(exchangeData.MapToEntity());
        }
    }
}