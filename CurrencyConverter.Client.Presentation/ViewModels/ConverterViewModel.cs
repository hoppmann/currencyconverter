﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using CurrencyConverter.Client.BusinessLogic.Interfaces;
using CurrencyConverter.Client.BusinessLogic.Models;

namespace CurrencyConverter.Client.Presentation.ViewModels
{
    class ConverterViewModel : PropertyChangedBase
    {
        public ConverterViewModel(
            ICurrencyConverter currencyConverter,
            IExchangeRatesProvider exchangeRatesProvider,
            IEventAggregator eventAggregator)
        {
            CurrencyConverter = currencyConverter;
            ExchangeRatesProvider = exchangeRatesProvider;
            EventAggregator = eventAggregator;
            SourceCurrencyValue = decimal.Zero;
            TargetCurrencyValue = decimal.Zero;
            LoadCurrencyList();

            EventAggregator.Subscribe(this);
        }

        public bool CurrencyIsLoading { get; set; }

        public CurrencyRateViewModel SelectedSourceCurrency { get; set; }

        public CurrencyRateViewModel SelectedTargetCurrency { get; set; }

        public decimal SourceCurrencyValue { get; set; }

        public decimal TargetCurrencyValue { get; set; }

        public ObservableCollection<CurrencyRateViewModel> SourceCurrencyList { get; set; }

        public ObservableCollection<CurrencyRateViewModel> TargetCurrencyList { get; set; }

        private ICurrencyConverter CurrencyConverter { get; }

        private IExchangeRatesProvider ExchangeRatesProvider { get; }

        public IEventAggregator EventAggregator { get; }

        private ExchangeRatesBusinessModel ExchangeRatesPresentationModel { get; set; }

        public void OnSelectedTargetCurrencyChanged()
        {
            ConvertCurrency();
        }

        public void OnSourceCurrencyValueChanged()
        {
            ConvertCurrency();
        }

        public void OnSelectedSourceCurrencyChanged()
        {
            var selection = new List<CurrencyRateViewModel> {SelectedSourceCurrency};

            TargetCurrencyList = new ObservableCollection<CurrencyRateViewModel>(SourceCurrencyList.Except(selection));
            ConvertCurrency();
        }

        public void ResetRates()
        {
            LoadCurrencyList();
        }

        private Task LoadCurrencyList()
        {
            CurrencyIsLoading = true;
            return Task.Run(() =>
            {
                if (ExchangeRatesPresentationModel != null)
                    return;

                ExchangeRatesPresentationModel = ExchangeRatesProvider.LoadExchangeData();

                var currencyRateViewModels =
                    ExchangeRatesPresentationModel.Rates.Select(x =>
                        new CurrencyRateViewModel(x.CurrencyName, x.Rate)).ToList();

                SourceCurrencyList = new ObservableCollection<CurrencyRateViewModel>(currencyRateViewModels);
                TargetCurrencyList = new ObservableCollection<CurrencyRateViewModel>(currencyRateViewModels);
                CurrencyIsLoading = false;
            });
        }

        private void ConvertCurrency()
        {
            if (SelectedSourceCurrency == null || SelectedTargetCurrency == null)
                return;

            var source = new CurrencyExchangeBusinessModel
            {
                CurrencyName = SelectedSourceCurrency.CurrencyName,
                Rate = SelectedSourceCurrency.Rate
            };
            var target = new CurrencyExchangeBusinessModel
            {
                CurrencyName = SelectedTargetCurrency.CurrencyName,
                Rate = SelectedTargetCurrency.Rate
            };
            Task.Run(() =>
            {
                TargetCurrencyValue =
                    CurrencyConverter.ConvertCurrencyValue(SourceCurrencyValue, source.CurrencyName, target.CurrencyName);
            });
        }
    }
}