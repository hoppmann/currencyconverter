﻿using System.Globalization;
using System.Windows.Controls;

namespace CurrencyConverter.Client.Presentation.ViewModels
{
    public class StringToDouble : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var result = 0.0;
            var canConvert = double.TryParse(value as string, out result);
            return new ValidationResult(canConvert, "Not a valid double");
        }
    }
}