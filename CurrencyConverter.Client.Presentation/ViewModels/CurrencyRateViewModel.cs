﻿namespace CurrencyConverter.Client.Presentation.ViewModels
{
    public class CurrencyRateViewModel
    {
        public CurrencyRateViewModel(string currencyName, decimal rate)
        {
            CurrencyName = currencyName;
            Rate = rate;
        }
        public string CurrencyName { get; private set; }
        public decimal Rate { get; private set; }
    }
}
