﻿using System;
using System.Windows;
using Autofac;
using Caliburn.Micro;
using CurrencyConverter.Client.BusinessLogic;
using CurrencyConverter.Client.Presentation.ViewModels;

namespace CurrencyConverter.Client.Presentation
{
    class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        private IContainer Container { get; set; }

        protected override void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ConverterViewModel>().SingleInstance();
            builder.RegisterType<CurrencyRateViewModel>();
            builder.RegisterType<WindowManager>().As<IWindowManager>();
            builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();

            BusinesLogicContainer.RegisterTypes(builder);

            Container = builder.Build();
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                if (Container.IsRegistered(serviceType))
                    return Container.Resolve(serviceType);
            }
            else
            {
                if (Container.IsRegisteredWithName(key, serviceType))
                    return Container.ResolveNamed(key, serviceType);
            }
            throw new Exception($"Could not locate any instances of contract {key ?? serviceType.Name}.");
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ConverterViewModel>();
        }
    }
}