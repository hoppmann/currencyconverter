﻿using System;
using CurrencyConverter.Client.DataAcces.Models;

namespace CurrencyConverter.Client.DataAcces.Interfaces
{
    public interface IApiAcces
    {
        bool DataAvailable();

        ExchangeRatesEntitie GetExchangeRates();
        ExchangeRatesEntitie GetSpecificExchangeRates(DateTime date, string baseCurrency);

        decimal GetExchangeResult(decimal sourceValue, string sourceCurrencyName,
            string targetCurrencyName);

        bool NewerDataAvailable(DateTime currentDateTime);
    }
}