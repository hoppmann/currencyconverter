﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Client.DataAcces.Models
{
    public class ExchangeRatesEntitie
    {
        public virtual int Id { get; set; }
        public virtual string Base { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual IList<CurrencyExchangeEntitie> Rates { get; set; }
    }
}