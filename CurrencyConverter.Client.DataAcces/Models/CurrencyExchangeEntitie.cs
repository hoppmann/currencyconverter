﻿namespace CurrencyConverter.Client.DataAcces.Models
{
    public class CurrencyExchangeEntitie
    {
        public virtual int Id { get; set; }
        public virtual string CurrencyName { get; set; }        
        public virtual decimal Rate { get; set; }        
    }
}