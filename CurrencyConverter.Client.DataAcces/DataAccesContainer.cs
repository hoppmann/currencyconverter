﻿using Autofac;
using CurrencyConverter.Client.DataAcces.Interfaces;

namespace CurrencyConverter.Client.DataAcces
{
    public static class DataAccesContainer
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<ApiAcces>().As<IApiAcces>();
        }
    }
}
