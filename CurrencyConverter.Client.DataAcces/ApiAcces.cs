﻿using System;
using System.Net.Http;
using CurrencyConverter.Client.DataAcces.Interfaces;
using CurrencyConverter.Client.DataAcces.Models;
using Newtonsoft.Json;

namespace CurrencyConverter.Client.DataAcces
{
    public class ApiAcces : IApiAcces
    {
        private readonly string _apiAdressBase = "http://localhost:55754/api";
        private const string StandartCurrencyBase = "USD";

        private TResult RequestFromApi<TResult>(string apiAdress)
        {
            using (var client = new HttpClient())
            {
                
                var result = client.GetAsync(apiAdress).Result;
                var statusCode = result.EnsureSuccessStatusCode().StatusCode; //TODO: USEABLE FOR UI FEEDBACK?

                var serialData = result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<TResult>(serialData);
            }
        }

        public string  ApiAdress { get; set; }


        private string ConfigApiAdress(ControllerType controller, string parameters)
        {
            var controllerName = controller.ToString().ToLower();

            ApiAdress = $"{_apiAdressBase}/{controllerName}?{parameters}";

            return ApiAdress;
        }

        private string ConfigApiParams(ControllerService service, DateTime date, string currency)
        {
            if(service != ControllerService.GetExchangeRate)
                throw new NotImplementedException();

            return $"{GetEchangeRatesParameter.Date.ToString().ToLower()}={date:yyyy-MM-dd}&" +
                   $"{GetEchangeRatesParameter.Base.ToString().ToLower()}={currency}";
        }

        private string ConfigApiParams(ControllerService service, decimal sourceValue, string sourceCurrencyName, string targetCurrencyName)
        {
            if(service != ControllerService.GetCalculationResult)
                throw new NotImplementedException();

            return $"{GetCalculationResultParameter.From.ToString().ToLower()}={sourceCurrencyName}&" +
                   $"{GetCalculationResultParameter.To.ToString().ToLower()}={targetCurrencyName}&" +
                   $"{GetCalculationResultParameter.Value.ToString().ToLower()}={sourceValue}";
 
        }

        private enum ControllerType
        {
            Calculator, ExchangeRates
        }

        private enum ControllerService
        {
            GetExchangeRate,
            GetCalculationResult
        }

        private enum GetEchangeRatesParameter
        {
            Base, Date
        }

        private enum GetCalculationResultParameter
        {
            Value, From, To
        }

        //TODO:SetToValidation

        public bool DataAvailable()
        {
            return true;
        }

        public ExchangeRatesEntitie GetExchangeRates()
        {
            return GetSpecificExchangeRates(DateTime.Today, StandartCurrencyBase);
        }

        public ExchangeRatesEntitie GetSpecificExchangeRates(DateTime date, string baseCurrency = StandartCurrencyBase)
        {
            var paramsString = ConfigApiParams(ControllerService.GetExchangeRate, date, StandartCurrencyBase);
            var adress = ConfigApiAdress(ControllerType.ExchangeRates, paramsString);

            return RequestFromApi<ExchangeRatesEntitie>(adress);
        }

        public decimal GetExchangeResult(decimal sourceValue, string sourceCurrencyName, string targetCurrencyName)
        {
            var paramsString = ConfigApiParams(ControllerService.GetCalculationResult, sourceValue, sourceCurrencyName, targetCurrencyName);
            var adress = ConfigApiAdress(ControllerType.Calculator, paramsString);

            return RequestFromApi<decimal>(adress);
        }

        //TODO:SetToValidation
        public bool NewerDataAvailable(DateTime currentDateTime)

        {
            return true;
        }
    }
 }