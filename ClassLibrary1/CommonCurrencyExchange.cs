﻿namespace CurrencyConverter.Common.Models
{
    public class CommonCurrencyExchange
    {
        public string CurrencyName { get; set; }
        public decimal Rate { get; set; }
    }
}