﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Common.Models
{
    public class CommonExchangeRate
    {
        public string Base { get; set; }
        public DateTime Date { get; set; }
        public IList<CommonCurrencyExchange> Rates { get; set; }
    }
}
