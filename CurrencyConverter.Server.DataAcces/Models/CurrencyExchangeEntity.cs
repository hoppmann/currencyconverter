﻿namespace CurrencyConverter.Server.DataAcces.Models
{
    public class CurrencyExchangeEntity
    {
        public virtual int Id { get; set; }
        public virtual string CurrencyName { get; set; }        
        public virtual decimal Rate { get; set; }        
    }
}