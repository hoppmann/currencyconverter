﻿using System;
using System.Collections.Generic;

namespace CurrencyConverter.Server.DataAcces.Models
{
    public class ExchangeRatesEntity
    {
        public virtual int Id { get; set; }
        public virtual string Base { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual IList<CurrencyExchangeEntity> Rates { get; set; }
    }
}