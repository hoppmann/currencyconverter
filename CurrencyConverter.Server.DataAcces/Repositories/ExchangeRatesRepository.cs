﻿using System;
using System.Linq;
using CurrencyConverter.Server.DataAcces.Interfaces;
using CurrencyConverter.Server.DataAcces.Models;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;
using NHibernate.Tool.hbm2ddl;

namespace CurrencyConverter.Server.DataAcces.Repositories
{
    public class ExchangeRatesRepository : IExchangeRatesRepository
    {
        private static string _connectionString = "Server=(LocalDB)\\MSSQLLocalDB;Initial Catalog=CurrencyDatabase";
        private static readonly ISessionFactory SessionFactory = CreateSessionFactory();

        public bool DataAvailable()
        {
            using (var session = SessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dataAvailable = session.Query<ExchangeRatesEntity>().ToList()
                        .Any();

                    transaction.Commit();
                    return dataAvailable;
                }
            }
        }

        public ExchangeRatesEntity Load(DateTime specificDateTime)
        {
            using (var session = SessionFactory.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    return session.Query<ExchangeRatesEntity>().Single(x => x.Date.Equals(specificDateTime));
                }
            }
        }

        public bool Save(ExchangeRatesEntity exchangeRatesEntity)
        {
            if (DbIsUpToDate(exchangeRatesEntity))
                return true;

            using (var session = SessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.SaveOrUpdate(exchangeRatesEntity);
                    
                    transaction.Commit();
                }
            }

            return true;
        }

        private static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(_connectionString))
                .Mappings(m =>
                {
                    m.FluentMappings.AddFromAssemblyOf<ExchangeRatesEntity>();
                    m.FluentMappings.AddFromAssemblyOf<CurrencyExchangeEntity>();
                })
                .ExposeConfiguration(BuildSchema)
                .BuildSessionFactory();
        }

        private static void BuildSchema(Configuration config)
        {
            new SchemaUpdate(config)
                .Execute(false, true);
        }

        private bool DbIsUpToDate(ExchangeRatesEntity comparExchangeRates)
        {
            using (var session = SessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbIsUpToDate =
                        session
                            .Query<ExchangeRatesEntity>()
                            .Where<ExchangeRatesEntity>(x => x.Date.Equals(comparExchangeRates.Date))
                            .ToList()
                            .Any();

                    transaction.Commit();

                    return dbIsUpToDate;
                }
            }
        }
    }
}