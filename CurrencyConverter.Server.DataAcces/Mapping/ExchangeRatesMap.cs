﻿using CurrencyConverter.Server.DataAcces.Models;
using FluentNHibernate.Mapping;

namespace CurrencyConverter.Server.DataAcces.Mapping
{
    public class ExchangeRatesMap : ClassMap<ExchangeRatesEntity>
    {
        public ExchangeRatesMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Base).Not.Nullable();
            Map(x => x.Date).Not.Nullable();
            HasMany(x => x.Rates).Cascade.All();
        }
    }
}
