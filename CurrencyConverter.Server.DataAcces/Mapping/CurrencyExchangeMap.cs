﻿using CurrencyConverter.Server.DataAcces.Models;
using FluentNHibernate.Mapping;

namespace CurrencyConverter.Server.DataAcces.Mapping
{
    public class CurrencyExchangeMap : ClassMap<CurrencyExchangeEntity>
    {
        public CurrencyExchangeMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.CurrencyName).Not.Nullable();
            Map(x => x.Rate).Not.Nullable();
        }
    }
}