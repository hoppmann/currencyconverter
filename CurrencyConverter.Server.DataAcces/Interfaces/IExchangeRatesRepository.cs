﻿using System;
using CurrencyConverter.Server.DataAcces.Models;

namespace CurrencyConverter.Server.DataAcces.Interfaces
{
    public interface IExchangeRatesRepository
    {
        bool DataAvailable();
        ExchangeRatesEntity Load(DateTime specificDateTime);
        bool Save(ExchangeRatesEntity exchangeRatesEntity);
    }
}