﻿using Autofac;
using CurrencyConverter.Server.DataAcces.Interfaces;
using CurrencyConverter.Server.DataAcces.Repositories;

namespace CurrencyConverter.Server.DataAcces
{
    public static class DataAccesContainer
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<ExchangeRatesRepository>().As<IExchangeRatesRepository>();
        }
    }
}
